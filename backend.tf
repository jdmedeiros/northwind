terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/23474808/terraform/state/production"
    lock_address = "https://gitlab.com/api/v4/projects/23474808/terraform/state/production/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/23474808/terraform/state/production/lock"
    username = "medeiros@enta.pt"
    password = "32Lop3a8jMssdV9wdbbV"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = "5"
  }
}