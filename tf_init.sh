#!/usr/bin/env bash
terraform init \
-backend-config="address=https://gitlab.com/api/v4/projects/ecfb472de10cf9de0988dcc48b4ea5a00ece005b/terraform/state/production" \
-backend-config="lock_address=https://gitlab.com/api/v4/projects/ecfb472de10cf9de0988dcc48b4ea5a00ece005b/terraform/state/production/lock" \
-backend-config="unlock_address=https://gitlab.com/api/v4/projects/ecfb472de10cf9de0988dcc48b4ea5a00ece005b/terraform/state/production/lock" \
-backend-config="username=medeiros@enta.pt" \
-backend-config="password=32Lop3a8jMssdV9wdbbV" \
-backend-config="lock_method=POST" \
-backend-config="unlock_method=DELETE" \
-backend-config="retry_wait_min=5"
